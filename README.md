# Blurt Account Recovery

## Website

https://recovery.blurtwallet.com

## Technology

- dHive
- jQuery
- Docker

## Building Image

To build and tag image

```bash
docker build -t tekraze/blurt-recovery:1.0.0 .
```

To run recovery app

```bash
sudo docker run -it --rm -d -p 8080:80 --name blurtrecovery blurt-recovery
```

## Using PreBuilt Image from DockerHub

https://hub.docker.com/r/balvinder294/blurt-recovery

pull image

```
docker pull balvinder294/blurt-recovery:1.0.0
```

To run recovery app

```bash
sudo docker run -it --rm -d -p 8080:80 --name blurtrecovery balvinder294/blurt-recovery
```

## Tutorial on how to use this

https://blurt.blog/blurtdevelopment/@saboin/account-recovery-or-dev-update-2022-02-03

## Contributing

Feel free to fork the repository and submit your changes.


----

Maintained by [@tekraze](https://blurt.blog/@tekraze). Vote for [@tekraze as Witness here](https://blurtwallet.com/~witnesses?highlight=tekraze)

## Personal Blog

Give a visit to [Tekraze](https://tekraze.com) for tech blog


## People Contributing to this Project

[@tekraze](https://blurt.blog/@tekraze)

[@saboin](https://blurt.blog/@saboin)
